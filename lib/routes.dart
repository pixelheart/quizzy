import 'package:quizzy/launch/launch.dart';
import 'login/login.dart';
import 'topics/topics.dart';
import 'about/about.dart';
import 'profile/profile.dart';

var appRoutes = {
  '/': (context) => const LaunchScreen(),
  '/login': (context) => const LoginScreen(),
  '/topics': (context) => const TopicScreen(),
  '/profile': (context) => const ProfileScreen(),
  '/about': (context) => const AboutScreen()
};
