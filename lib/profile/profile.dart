import 'package:flutter/material.dart';
import 'package:quizzy/services/auth.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: AuthService().signOut,
              child: const Icon(
                Icons.logout,
                size: 26,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
